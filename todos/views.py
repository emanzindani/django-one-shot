from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import (
    TodoListForm,
    TodoListUpdateForm,
    TodoItemForm,
    TodoItemUpdateForm,
)

# Create your views here.
def show_todolist(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}
    return render(request, "todos/list.html", context)


def show_a_todolist(request, id):
    todolist = TodoList.objects.get(id=id)
    tasks = todolist.items.all()
    context = {
        "todolist": todolist,
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_form = form.save()
            return redirect("todo_list_detail", id=new_form.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def update_todolist(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListUpdateForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
        else:
            form = TodoListUpdateForm(instance=model_instance)
        context = {"form": form}
        return render(request, "todos/updatelist.html", context)


def delete_todolist(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/deletelist.html")


def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            get_list_id = form.save()
            return redirect("todo_list_detail", id=get_list_id.list.id)
        else:
            form = TodoItemForm()
        context = {"form": form}
        return render(request, "todos/createitem.html", context)


def update_todoitem(request, id):
    model_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemUpdateForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemUpdateForm(instance=model_instance)

    context = {"form": form}

    return render(request, "todos/updateitem.html", context)
